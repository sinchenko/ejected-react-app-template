module.exports = {
  extends: ['stylelint-config-standard', 'stylelint-config-prettier'],
  globals: {},
  rules: {
    'font-family-no-missing-generic-family-keyword': null,
    'at-rule-no-unknown': [
      true,
      {
        ignoreAtRules: [
          'at-root',
          'content',
          'else',
          'extend',
          'function',
          'if',
          'include',
          'mixin',
          'return',
          'warn',
          'error',
          'each',
          'for',
        ],
      },
    ],
  },
};
